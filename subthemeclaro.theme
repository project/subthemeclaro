<?php

/**
 * @file
 * Functions to support theming in the SubThemeClaro theme.
 */

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Element;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;


/**
 * Implements hook_preprocess_HOOK() for page.
 */
function subthemeclaro_preprocess_page(&$variables) {
  // Required for allowing subtheming Subthemeclaro.
  $activeThemeName = \Drupal::theme()->getActiveTheme()->getName();
  $variables['active_admin_theme'] = $activeThemeName;
}

/**
 * Implements hook_theme_suggestions_HOOK_alter for blocks.
 */
function subthemeclaro_theme_suggestions_block_alter(&$suggestions, $variables) {
  // https://www.drupal.org/docs/theming-drupal/creating-sub-themes

  // Load theme suggestions for blocks from parent theme.
  foreach ($suggestions as &$suggestion) {
    $suggestion = str_replace('subthemeclaro_', 'claro_', $suggestion);
  }
}

/**
 * Implements hook_preprocess_HOOK() for page_alter.
 */
function subthemeclaro_theme_suggestions_page_alter(&$suggestions, $variables) {
  $path = \Drupal::requestStack()->getCurrentRequest()->getPathInfo();

  if ($path != '/') {
    $path = trim($path, '/');
    $arg = str_replace(["/", '-'], ['_', '_'], $path);
    $suggestions[] = 'page__' . $arg;
  }
}


/**
 * Implements form_alter() for forms.
 */
function subthemeclaro_theme_suggestions_form_alter(array &$suggestions, array $variables) {
  $suggestions[] = 'form__' . str_replace('-', '_', $variables['element']['#id']);
}

